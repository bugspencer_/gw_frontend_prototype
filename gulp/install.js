'use strict';

var gulp = require('gulp'),
	conf = require('./conf'),
	fs = require('fs');


gulp.task('install', [],function(){
	fs.exists('./'+conf.files.localJsHint, function (exists) {
		if(!exists){
			fs.createReadStream('./'+conf.files.localJsHintSample)
				.pipe(fs.createWriteStream('./'+conf.files.localJsHint));
		}
	});
});