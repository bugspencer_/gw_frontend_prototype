(function(gamewheel) {
  'use strict';

  gamewheel.connect('GameWheel',
    'Dashboard',
    'ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ngRoute'
  );

})(window.gw, window.angular);

