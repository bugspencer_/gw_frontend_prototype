(function(gamewheel) {
  'use strict';

  gamewheel.connect('GameWheel', function () {
    this.config(routeConfig);
  });

  routeConfig.$inject = ['$routeProvider', '$locationProvider'];
    function routeConfig($routeProvider, $locationProvider) {

      $routeProvider
        .when('/', {
          redirectTo: '/dashboard'
        })
        // fallback
        .otherwise({
          redirectTo: '/'
        });

      $locationProvider.html5Mode(true);
    }

})(window.gw, window.angular);
