(function (gamewheel) {
    'use strict';

  gamewheel.connect('GameWheel', function(){
    this.controller('AppController', AppController);
  });

  AppController.$inject = [];
    function AppController (){

      var vm = this;

      vm.test = true;
    }

})(window.gw, window.angular);
