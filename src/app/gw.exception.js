(function (window){
  'use strict';

  var gamewheel = window.gw|| (window.gw = {});
  var angular = window.angular;

  gamewheel.exception = {};

  function ApplictaionNotFoundException (_appName){
    console.log('Application: ' + _appName + ' not found');
  }

  function ModuleNotFoundException (_moduleName){
    console.log('Module: ' + _moduleName + ' not found');
  }

  angular.copy({
    ApplictaionNotFound : ApplictaionNotFoundException,
    ModuleNotFound : ModuleNotFoundException
  },gamewheel.exception);

})(window);
