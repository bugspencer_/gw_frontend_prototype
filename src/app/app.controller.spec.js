(function() {
  'use strict';

  describe('controllers', function(){

    beforeEach(module('GameWheel'));

    it('should find the controller', inject(function($controller) {
      var vm = $controller('AppController');

      expect(vm.test).toBeTruthy();
    }));
  });
})();
