/**
 * Created by crispy on 23/11/15.
 */


(function() {
  'use strict';

  describe('controllers', function(){

    beforeEach(module('Dashboard'));

    it('should find the controller', inject(function($controller) {
      var vm = $controller('DashboardController');

      expect(vm.test).toBeTruthy();
    }));
  });
})();

