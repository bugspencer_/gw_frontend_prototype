(function(gamewheel) {
  'use strict';

  gamewheel.connect('Dashboard', function () {
    this.config(routeConfig);
  }, 'ngRoute');

  routeConfig.$inject = ['$routeProvider'];
    function routeConfig($routeProvider) {

      $routeProvider
        .when('/dashboard', {
          controller : 'DashboardController',
          controllAs : 'dashboard',
          templateUrl : 'app/dashboard/dashboard.html'
        });
    }

})(window.gw, window.angular);
