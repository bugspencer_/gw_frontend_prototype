(function (gamewheel) {
  'use strict';

  gamewheel.connect('Dashboard', function () {

    this.controller('DashboardController', DashboardController);

  });

  DashboardController.$inject = [];
	  function DashboardController (){
	    var vm = this;
	    vm.test = true;
	  }

})(window.gw, window.angular);
