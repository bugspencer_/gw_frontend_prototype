(function(gamewheel) {
  'use strict';

  gamewheel.connect('GameWheel', function () {
    this.run(runBlock);
  });

  runBlock.$inject = ['$log'];
    function runBlock($log) {
      $log.debug('runBlock end');
    }

})(window.gw, window.angular);
