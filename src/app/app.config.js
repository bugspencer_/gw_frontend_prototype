(function(gamewheel) {
  'use strict';

  gamewheel.connect('GameWheel', function () {
    this.config(config);
  });

  config.$inject = ['$logProvider'];
	function config($logProvider) {
		// Enable log
		$logProvider.debugEnabled(false);
	}

})(window.gw, window.angular);
