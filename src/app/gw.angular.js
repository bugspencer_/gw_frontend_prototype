(function(window){
  'use strict';

  var angular = window.angular,

    /**
     * public GameWheel Object
     * announced as wg
     * @todo check if there is a namespace conflict
     * @type {Object}
     */
    gamewheel = window.gw|| (window.gw = {}),

    /**
     * list of module dependencies
     * @type {Object}
     */
    moduleDependencies = {},

    /**
     * callback functions
     * @type {Object}
     */
    moduleCallbacks = {},

    /**
     * list of all available modules
     * @type {Object}
     * */
    modules = {},

    /**
     * appliaction module
     * @type {ng.module}
     */
    appModule = null,

    /**
     * application wrapper
     * @type {ng.module}
     */
    app = null,

    /**
     * application name
     * @type {string}
     */
    appName = 'GameWheel';


  gamewheel.connect = connect;
  gamewheel.boot = boot;


  /**
   * connects a module to the application
   * @param _moduleName
   * @param _moduleCallback
   * @param list of module dependencies
   */
  function connect (_moduleName, _moduleCallback){

    /* get the list of dependencies */
    var connectorDependencies = Array.prototype.slice.call(arguments, 2);

    /* get connectionCallback for this `_moduleName` or create an array */
    var connectionCallbacks = (moduleCallbacks[_moduleName] || []);

    /* its a real connector function for this module */
    if(angular.isFunction(_moduleCallback)){
      connectionCallbacks.push(_moduleCallback); }

    /* or its just a module proxy */
    else if(angular.isString(_moduleCallback)){
      connectorDependencies.push(_moduleCallback); }

    /* write back the connectionCallbacks */
    moduleCallbacks[_moduleName] = connectionCallbacks;

    /* merge connectorDependencies in moduleDependencies */
    moduleDependencies[_moduleName] = (moduleDependencies[_moduleName] || []).concat(connectorDependencies || []);

  }


  /**
   * to boot the application
   * @param _appName
   * @param _config
   * @param _element
   */
  function boot (_config, _element){

    /* ng.injector, maybe its needed */
    var injector = angular.injector(['ng']),
        element = angular.isDefined(_element) ? _element : injector.get('$document');

    var config = angular.extend({
      appName : appName
    }, _config);

    /* create all modules */
    angular.forEach(moduleDependencies, createModule);

    /* run all connector callbacks */
    angular.forEach(moduleCallbacks, runConnectorCallback);

    /* pick the application module */
    try {
      appModule = getModule(config.appName);
    } catch(err) {
      throw new gamewheel.exception.ApplictaionNotFound(config.appName);
    }


    /* create application */
    app = angular.module('app', [appModule.name]);

    app.constant('APP_CONFIG', angular.copy(config, {}));

    // boot application module bind on document
    angular.bootstrap(angular.element(element), [app.name]);
  }


  /**
   * create a module
   * @param _dependencies
   * @param _moduleName
   */
  function createModule (_dependencies, _moduleName){
    modules[_moduleName] = angular.module(_moduleName, _dependencies); }

  /**
   *
   * @param _connectionCallbacks
   * @param _moduleName
   */
  function runConnectorCallback (_connectionCallbacks, _moduleName){

    var module = getModule(_moduleName);

    angular.forEach(_connectionCallbacks, function(_connectionCallback){
      _connectionCallback.call(module, window); });

  }

  /**
   * returns a module by `_moduleName`
   * @param _moduleName
   * @returns {*}
   */
  function getModule(_moduleName){

    if (!modules[_moduleName]){
      throw new gamewheel.exception.ModuleNotFound(_moduleName); }

    return modules[_moduleName];

  }

})(window);
